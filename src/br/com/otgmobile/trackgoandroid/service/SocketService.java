package br.com.otgmobile.trackgoandroid.service;

import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import br.com.otgmobile.trackgoandroid.database.DatabaseHelper;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.socket.impl.EventType;
import br.com.otgmobile.trackgoandroid.socket.impl.SocketManagerDelegate;
import br.com.otgmobile.trackgoandroid.util.BroadCastDictionary;
import br.com.otgmobile.trackgoandroid.util.LogUtil;
import br.com.otgmobile.trackgoandroid.util.Session;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;


public class SocketService extends Service{

	public static final String SOCKET_SERVICE_INTENT = "SOCKET_SERVICE_INTENT";
	private final SocketIOBinder binder = new SocketIOBinder();
	private Socket socketIO;
	private Handler mHandler;
	private SocketManagerDelegate socketDelegate;
	
	public static void startService(Context context) {
		context.startService(new Intent(SOCKET_SERVICE_INTENT));
	}

	public static void stopService(Context context) {
		context.stopService(new Intent(SOCKET_SERVICE_INTENT));
	}

	private SocketManagerDelegate socketDelegate() {
		if ( socketDelegate == null ) {
			socketDelegate = new SocketManagerDelegate(getApplicationContext());
		}

		return socketDelegate;
	}
	
	public interface ISocketService {
		public void sendDevicesID();
	}
	
	public ISocketService mSocketService = new ISocketService() {
		
		public void sendDevicesID() {
			emitDevicesToNode();
		}
	};
	private BroadcastReceiver conectionStatusReciever = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			emitDevicesToNode();
		}
	};


	public class SocketIOBinder extends Binder {
		public ISocketService getService(){
			return mSocketService;
		}
	}

	@Override
	public IBinder onBind(Intent it) {
		return binder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mHandler = new Handler();		
		try {
			socketIO = IO.socket(Session.getSocketServer(getApplicationContext()));
		} catch (URISyntaxException e) {
			Log.e("SOCKET_ERROR", e.getMessage());
		}
		prepareSocketIO();
		mHandler.post(mScheduler);
		registerReceiver(conectionStatusReciever, new IntentFilter(BroadCastDictionary.SEND_DEVICE.getValue()));
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		disconnectIOSocket();
		unregisterReceiver(conectionStatusReciever);
		release();
	}

	private void disconnectIOSocket() {
		if ( socketIO != null ) {
			socketIO.disconnect();
		}
	}

	private void release() {
		mHandler.removeCallbacks(mScheduler);
		mHandler = null;
		socketIO = null;
	}

	/**
	 * Scheduler to check if IOSocket is alive(isConnected) otherwise trying connect.
	 */
	private final Runnable mScheduler = new Runnable() {
		public void run() {
			connect();
		}
	};

	private void connect() {
		LogUtil.i("Conectando com o SocketIO"); 
    	socketIO.connect();
	}
	
	private void prepareSocketIO(){
		
		socketIO.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
    		public void call(Object... args) {
    			onConnect();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
    		
    		public void call(Object... args) {
    			System.out.println("Caiu a conexão com o SocketIO"); 
				onDisconnect();
				connect();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
    		
    		public void call(Object... args) {
    			System.out.println("Erro de conexão SocketIO"); 
    			onConnectFailure();
    			open();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
    		
    		public void call(Object... args) {
    			System.out.println("Erro de conexão SocketIO"); 
    			onConnectFailure();
    			open();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_RECONNECT_ERROR, new Emitter.Listener() {
    		
    		public void call(Object... args) {
    			System.out.println("Reconect error SocketIO"); 
    			onConnectFailure();
    			open();
				
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_RECONNECT_FAILED, new Emitter.Listener() {
    		
    		public void call(Object... args) {
    			System.out.println("Reconect failed SocketIO"); 
    			onConnectFailure();
    			open();
    		}

    	});
    	
    	socketIO.on(EventType.EVENT_HISTORY.getValue(), new Emitter.Listener() {

  		  public void call(Object... args) {
  			  JSONObject object;
  			try {
  				object = new JSONObject(args[0].toString());
  				socketDelegate().delegateEvent(EventType.EVENT_HISTORY.getValue(), object);
  			} catch (JSONException e) {
  				LogUtil.e("SOCKET IO ERROR " + EventType.EVENT_HISTORY.getValue(), e);
  			}
  		  }

  	    });
    	
	}
	
	public void open() {
		Log.i("SOCKET_CONNECTED", "Connected with IOSocket...");
    	socketIO.open();
    }
	
	private void onConnect() {
		Log.i("SOCKET_CONNECTED", "Connected with IOSocket...");
		JSONObject obj;
		try {
			obj = new JSONObject().accumulate("token", Session.getToken(getApplicationContext()));
			emitEventToNode(EventType.ATUALIZA_SESSAO, obj);
		} catch (JSONException e) {
			LogUtil.e("SOCKET IO ERROR " + EventType.EVENT_HISTORY.getValue(), e);
		}
		
	}
	
	private boolean emitEventToNode(final EventType eventType, final JSONObject jsonObject) {
		
		try {
			socketIO.emit(eventType.getValue(), jsonObject);
			return true;
		}catch (Exception e) {
			LogUtil.e("Erro enviando token para o node", e);
		}
		
		return false;
	}

	
	private void onConnectFailure() {
		Log.i("SOCKET_CONNECTION", "Connection failure with IOSocket...");
	}
	
	private void onDisconnect() {
		Log.i("SOCKET_DISCONNECTED", "Disconnected from IOSocket...");
	}

	private void emitDevicesToNode(){
		try {
			List<Device> devices = DatabaseHelper.getDatabase(SocketService.this).getDao(Device.class).queryForAll();
			List<Long> ids = new ArrayList<Long>();
			for(Device device :devices){
				ids.add(device.getId());
			}
			JSONObject jObject = new JSONObject();
			jObject.accumulate("devices", ids).accumulate("token", Session.getToken(getApplicationContext()));
			emitEventToNode(EventType.ENVIA_DEVICES, jObject);
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar devices do banco de dados", e);
		}catch (JSONException e) {
			LogUtil.e("erro ao transformar lista de devices em json", e);
		}
	}


}
