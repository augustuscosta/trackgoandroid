package br.com.otgmobile.trackgoandroid.database;

import java.sql.SQLException;

import br.com.otgmobile.trackgoandroid.util.LogUtil;

import com.google.inject.Provider;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class DAOProvider<T> implements Provider<T> {
    protected ConnectionSource conn;
    protected Class<T> clazz;

    public DAOProvider( ConnectionSource conn, Class<T> clazz ) {
        this.conn = conn;
        this.clazz = clazz;
    }


    public T get() {
        try {
			return DaoManager.createDao( conn, clazz  );
		} catch (SQLException e) {
			LogUtil.e("Erro ao criar Dao", e);
			return null;
		}
    }
}