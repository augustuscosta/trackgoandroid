package br.com.otgmobile.trackgoandroid.database;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.Event;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.model.TrackerData;
import br.com.otgmobile.trackgoandroid.model.User;
import br.com.otgmobile.trackgoandroid.util.LogUtil;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	
	private static final String DATABASE_NAME = "TrackGO.db";
	private static final int DATABASE_VERSION = 1;
	private static DatabaseHelper databaseHelper;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		try {
			TableUtils.createTable(connectionSource, User.class);
			TableUtils.createTable(connectionSource, Device.class);
			TableUtils.createTable(connectionSource, TrackerData.class);
			TableUtils.createTable(connectionSource, Event.class);
			TableUtils.createTable(connectionSource, EventHistoryDetail.class);
		} catch (SQLException e) {
			LogUtil.e(DatabaseHelper.class.getName(), e);
		}finally{
			DaoManager.clearDaoCache();
		}
		
	}
	
	public static OrmLiteSqliteOpenHelper getDatabase(Context context){
		if(databaseHelper == null){
			databaseHelper = new DatabaseHelper(context);
		}
		
		return databaseHelper;
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

}
