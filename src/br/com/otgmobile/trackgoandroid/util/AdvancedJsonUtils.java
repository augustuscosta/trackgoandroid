package br.com.otgmobile.trackgoandroid.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.DeviceType;
import br.com.otgmobile.trackgoandroid.model.Event;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.model.TrackerData;

import com.google.gson.Gson;
import com.j256.ormlite.dao.ForeignCollection;

public class AdvancedJsonUtils {
	
	
	private static Gson gson;

	static {
		gson = new Gson();
	}

	@SuppressWarnings("unchecked")
	public static Device returnDeviceWithTrackerDataFromJson(JSONObject json) throws Exception{
		Device device = new Device();
		if(json.has("id"))
			device.setId(Long.parseLong(json.get("id").toString()));
		if(json.has("code"))
			device.setCode(json.getString("code"));
		if(json.has("description"))
			device.setDescription(json.getString("description"));
		if(json.has("enabled"))
			device.setEnabled(json.getBoolean("enabled"));
		if(json.has("name"))
			device.setName(json.getString("name"));
		if(json.has("deviceType"))
			device.setDeviceType(DeviceType.fromName(json.getString("deviceType")));
		if(json.has("lastPosition")){
			device.setTrackerData(returnTrackerDataFromJson(json.getJSONObject("lastPosition")));
			device.getTrackeData().setDevice(device);
		}
		if(json.has("positions"))
			device.setPositions((ForeignCollection<TrackerData>)returnListPositionsFromJsonArray(json.getJSONArray("positions")));
		return device;
	}

	public static List<TrackerData> returnListPositionsFromJsonArray(JSONArray jsonArray)  throws Exception{
		List<TrackerData> positions = new ArrayList<TrackerData>();
		for (int i = 0; i < jsonArray.length(); i++) {
			positions.add(returnTrackerDataFromJson(jsonArray.getJSONObject(i)));
		}
		return positions;
	}

	public static TrackerData returnTrackerDataFromJson(JSONObject json) throws JSONException, ParseException {
		TrackerData trackerData= new TrackerData();
		if(json.has("id"))
			trackerData.setId(json.getInt("id"));
		if(json.has("altitude"))
			trackerData.setAltitude(json.getInt("altitude"));
		if(json.has("course"))
			trackerData.setCourse(json.getInt("course"));
		if(json.has("latitude"))
			trackerData.setLatitude(json.getDouble("latitude"));
		if(json.has("longitude"))
			trackerData.setLongitude(json.getDouble("longitude"));
		if(json.has("moving"))
			trackerData.setMoving(json.getBoolean("moving"));
		if(json.has("panic"))
			trackerData.setPanic(json.getBoolean("panic"));
		if(json.has("ignition"))
			trackerData.setIgnition(json.getBoolean("ignition"));
		if(json.has("speed"))
			trackerData.setSpeed(json.getDouble("speed"));
				if(json.has("date"))
				trackerData.setDate(returnDateFromJsonString(json.getString("date")));
		return trackerData;
	}

	public static Date returnDateFromJsonString(String string) throws ParseException {
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = formater.parse(string);
		return date;
	}
	
	public static EventHistoryDetail returnEventHistoryDetailFromJSon(JSONObject json) throws Exception{
		EventHistoryDetail eventHistoryDetail = new EventHistoryDetail();
		Gson gson = new Gson();
		if(json.has("id"))
		eventHistoryDetail.setId(json.getLong("id"));
		if(json.has("metricDate"))
		eventHistoryDetail.setMetricDate(returnDateFromJsonString(json.get("metricDate").toString()));
		if(json.has("latitude"))
		eventHistoryDetail.setLatitude((float)json.getDouble("latitude"));
		if(json.has("longitude"))
		eventHistoryDetail.setLongitude((float)json.getDouble("longitude"));
		if(json.has("device"))
		eventHistoryDetail.setDevice(gson.fromJson(json.get("device").toString(), Device.class));
		if(json.has("event")){
			Event event = gson.fromJson(json.get("event").toString(), Event.class);
			eventHistoryDetail.setEvent(event);			
		}
		
		return eventHistoryDetail;
	}
	
	

	public static <T> List<T> fromGson(final String rootObjectName, Class<T> clazz, final JSONObject... jsonObjects) {
		if (jsonObjects == null || jsonObjects.length == 0)
			return Collections.emptyList();

		final JSONArray jsonArray;
		List<T> toReturn = new ArrayList<T>();
		
		try {
			jsonArray = jsonObjects[0].getJSONArray(rootObjectName);

			if (jsonArray == null)
				return null;

			for (int i = 0; i < jsonArray.length(); i++) {
				toReturn.add(gson.fromJson(jsonArray.getJSONObject(i).toString(), clazz));
			}
		} catch (JSONException e) {
			LogUtil.e("Erro convertendo lista json", e);
			return null;
		}

		return toReturn;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object fromGson(final String rootObjectName,
			final Class clazz, final JSONObject jsonObject) {
		try {
			if (!jsonObject.has(rootObjectName)) {
				Log.w("GSON-Util", "Does not exist object name = "
						+ rootObjectName);
				return null;
			}

			return gson.fromJson(jsonObject.getJSONObject(rootObjectName)
					.toString(), clazz);
		} catch (Throwable e) {
			Log.e("GSON-Util", "Erro when trying convert(" + rootObjectName
					+ ") to - " + clazz.getSimpleName());
		}

		return null;
	}
	
	public static JSONObject getJsonObjectFromSocket(String rootObjectName,String response) throws JSONException{
    	JSONObject jObject = new JSONObject(response);
    	return jObject.getJSONObject(rootObjectName);
    }

}
