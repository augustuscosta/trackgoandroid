 package br.com.otgmobile.trackgoandroid.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public class Session {
	

	public static final String PREFS 						 = "SESSION_PREFS";
	public static final String SERVER_PREFS_KEY 			 = "SERVER_PREFS_KEY";
	public static final String SOCKET_SERVER_PREFS_KEY 		 = "SOCKET_SERVER_PREFS_KEY";
	public static final String TOKEN_PREFS_KEY 				 = "REST_TOKEN_PREFS_KEY";
	private static final String LAST_SYNC					 = "LAST_SYNC";

	public static final String SOCKET_CONECTION_STATUS 	     = "SOCKET_CONNECTION_STATUS";


	private static SharedPreferences settings;

	private static SharedPreferences getSharedPreferencesInstance(Context context){
		if(settings == null){
			settings = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		}
		return settings;
	}
	
	public static String getDeviceId(Context context){
		TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String szImei = TelephonyMgr.getDeviceId();
		return szImei;
	}
	
	public static void setServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SERVER_PREFS_KEY, server);
		editor.commit();
	}
	
	public static String getServer(Context context){				
		return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "http://54.84.198.243:8080/trackgoweb");
	}
	
	public static void setSocketServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SOCKET_SERVER_PREFS_KEY, server);
		editor.commit();
	}
	
	public static String getSocketServer(Context context){
		return getSharedPreferencesInstance(context).getString(SOCKET_SERVER_PREFS_KEY, "http://54.84.198.243:9090");
	}
	
	public static String getToken(Context context){
		return getSharedPreferencesInstance(context).getString(TOKEN_PREFS_KEY, null);
	}
	
	public static void setToken(String token,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(TOKEN_PREFS_KEY, token);
		editor.commit();
	}
	
	public static void clearSession(Context context){
		Session.setToken("", context);
	}
	
	public static void setLastSync(Context context, Long date){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor= settings.edit();
		editor.putLong(LAST_SYNC, date);
		editor.commit();
	}
	
	public static Long getLastSync(Context context){
		return getSharedPreferencesInstance(context).getLong(LAST_SYNC,  -1);		
	}

}
