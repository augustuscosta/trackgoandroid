package br.com.otgmobile.trackgoandroid.util;

public class StringUtil {

	/**
	 * @param value
	 * @return True if value is not null and trim().length() > 0, otherwise false.
	 */
	public static boolean isValid(final String value) {
		return value != null && value.trim().length() > 0;
	}
	
}

