package br.com.otgmobile.trackgoandroid.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.activity.EventLocaterMapActivity;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;


public class NotificationUtil {
	
	public static final int NOTIFICATION_ID = 1;

	
	public static void createNewOcurenceNotification(EventHistoryDetail eventHistoryDetail, Context context){
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		CharSequence contentTitle = context.getString(R.string.app_name);
		CharSequence contentText = getNewOcurrenceDescription(eventHistoryDetail, context);
		Notification notification = new Notification(R.drawable.ocorrencia_vermelho, contentText, System.currentTimeMillis());
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, getNewOcurrenceIntent(eventHistoryDetail, context), 0);
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		long[] vibrate = {0,100,200,300};
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.vibrate = vibrate;
		notification.defaults |= Notification.DEFAULT_LIGHTS;
		notification.ledARGB = 0xff00ff00;
		notification.ledOnMS = 300;
		notification.ledOffMS = 1000;
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;
		mNotificationManager.notify(NOTIFICATION_ID, notification);
	}
	
	private static Intent getNewOcurrenceIntent(EventHistoryDetail eventHistoryDetail, Context context){
		Intent intent = new Intent(context, EventLocaterMapActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, eventHistoryDetail);
		intent.putExtras(bundle);
		return intent;
	}
	
	private static String getNewOcurrenceDescription(EventHistoryDetail eventHistoryDetail, Context context){
		String description = "";
		if(eventHistoryDetail.getEvent()!=null && eventHistoryDetail.getEvent().getDescription()!= null){
			description = eventHistoryDetail.getEvent().getDescription();
		}
		
		return context.getString(R.string.event) + ": " + description;
	}
	
	public static void removeNotification(Context context){
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(NOTIFICATION_ID);
	}
	

}
