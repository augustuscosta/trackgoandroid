package br.com.otgmobile.trackgoandroid.util;

public enum BroadCastDictionary {
	
	INVALID_TOKEN("invalid_token"),
	SEND_DEVICE("send_devices"),
	NEW_EVENT("new_event");
	
	private BroadCastDictionary(String value) {
		this.value = value;
	}
	
	private String value;

	public String getValue() {
		return value;
	}

}
