package br.com.otgmobile.trackgoandroid.util;

import br.com.otgmobile.trackgoandroid.R;

public class ConstUtil {
	
	public static final int INVALID_TOKEN = 401;
	public static final int LOGIN = R.layout.main;
	public static final int DEVICE_DETAIL = 11;
	public static final int COMANDO = 12;
	public static final int EVENT_MAP = 13;
	public static final int EVENT_DETAIL = 14;
	public static final String SERIALIZABLE_KEY = "serializable_key";
	public static final String FINISH_ACTIVITY = "FINISH_ACTIVITY";
	public static final String RESULT_CODE = "resultCode";

}
