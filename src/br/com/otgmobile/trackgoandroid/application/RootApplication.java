package br.com.otgmobile.trackgoandroid.application;

import android.app.Application;
import android.content.Context;
import br.com.otgmobile.trackgoandroid.util.LogUtil;

public class RootApplication extends Application {
	
	
	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		LogUtil.initLog(base, null);
	}

}
