package br.com.otgmobile.trackgoandroid.model;

public enum TimeZone {
	
	GMT_12("-12:00",1),
	GMT_11("-11:00",2),
	GMT_10("-10:00",3),
	GMT_9("-9:00",4),
	GMT_8("-8:00",5),
	GMT_7("-7:00",6),
	GMT_6("-6:00",7),
	GMT_5("-5:00",8),
	GMT_4("-4:00",9),
	GMT_3("-3:00",10),
	GMT_2("-2:00",11),
	GMT_1("-1:00",12),
	GMT("0:00",13),
	GMT1("+1:00",14),
	GMT2("+2:00",15),
	GMT3("+3:00",16),
	GMT4("+4:00",17),
	GMT5("+5:00",18),
	GMT6("+6:00",19),
	GMT7("+7:00",20),
	GMT8("+8:00",21),
	GMT9("+9:00",22),
	GMT10("+10:00",23),
	GMT11("+11:00",24),
	GMT12("+12:00",25),
	GMT13("+13:00",26);
	
	private String label;
	private int value;
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}


	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}

	TimeZone(String label, int value){
		this.label = label;
		this.value = value;
		
	}
	
	public static TimeZone fromValue(int value){
		for (TimeZone timeZone : TimeZone.values()) {
			if(timeZone.getValue() == value) return timeZone;
			
		}
		
		return null;
	}
	
	public static TimeZone fromLabel(String label){
		for (TimeZone timeZone : TimeZone.values()) {
			if(timeZone.getLabel().equals(label)) return timeZone;
			
		}
		
		return null;
	}
	
}
