package br.com.otgmobile.trackgoandroid.model;

import java.io.Serializable;
import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="event_historyDetail")
public class EventHistoryDetail implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3775628140258477031L;

	@DatabaseField(id = true)
	private Long id;
	
		
	@DatabaseField
	private Date metricDate;
	
	@DatabaseField
	private Float latitude;
	
	@DatabaseField
	private Float longitude;
	
	@DatabaseField(foreign= true,foreignAutoRefresh=true,foreignColumnName="id")
	private Event event;
	
	@DatabaseField(foreign= true, foreignAutoRefresh=true,foreignColumnName="id")
	private Device device;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}


	public Date getMetricDate() {
		return metricDate;
	}

	public void setMetricDate(Date metricDate) {
		this.metricDate = metricDate;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}


	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public EventHistoryDetail(){
		
	}
	
}
