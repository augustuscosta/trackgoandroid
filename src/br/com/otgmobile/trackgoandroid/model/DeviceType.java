package br.com.otgmobile.trackgoandroid.model;

public enum DeviceType {

	TK10X,
	ASPICORE,
	MAXTRACK,
	SKYPATROL,
	NIAGARA2,
	NIAGARAHW6;
	
	public static DeviceType fromName(String name){
		if(name == null) return null;
		for(DeviceType deviceType :DeviceType.values()){
			if(name.equals(deviceType.name())) return deviceType;
		}
		return null;
	}
}
