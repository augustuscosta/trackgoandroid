package br.com.otgmobile.trackgoandroid.model;

import java.io.Serializable;
import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable
public class TrackerData implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7046915842349169911L;

	@DatabaseField(generatedId = true)
	private long id;
	
	@DatabaseField
	private Double latitude;
	
	@DatabaseField
	private Double longitude;
	
	@DatabaseField
	private Integer altitude;
	
	@DatabaseField
	private Integer course;
	
	@DatabaseField
	private Double speed;
	
	@DatabaseField
	private Boolean moving;
	
	@DatabaseField
	private Date date;
	
	@DatabaseField
	private Boolean ignition;
	
	@DatabaseField
	private Boolean panic;
	
	@DatabaseField(columnName="device_id",foreign=true,foreignAutoRefresh=true,foreignColumnName="id",foreignAutoCreate=true)
	private Device device;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Integer getAltitude() {
		return altitude;
	}
	
	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}
	
	public Integer getCourse() {
		return course;
	}
	
	public void setCourse(Integer course) {
		this.course = course;
	}
	
	public Double getSpeed() {
		return speed;
	}
	
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	
	public Boolean getMoving() {
		return moving;
	}
	
	public void setMoving(Boolean moving) {
		this.moving = moving;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Boolean getIgnition() {
		return ignition;
	}
	
	public void setIgnition(Boolean ignition) {
		this.ignition = ignition;
	}
	
	public Boolean getPanic() {
		return panic;
	}
	
	public void setPanic(Boolean panic) {
		this.panic = panic;
	}
	
	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public TrackerData(){
		//For use with orm
	}
	

}
