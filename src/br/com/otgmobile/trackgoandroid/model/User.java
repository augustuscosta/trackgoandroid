package br.com.otgmobile.trackgoandroid.model;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;

public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4906290859360532419L;

	@DatabaseField(id = true)
	private long id;
	
	@DatabaseField
	private String nome;
	
	@DatabaseField
	private String senha;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public User(){
		// for use with orm
	}
	

}
