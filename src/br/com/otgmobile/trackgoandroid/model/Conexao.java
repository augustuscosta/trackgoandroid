package br.com.otgmobile.trackgoandroid.model;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.otgmobile.trackgoandroid.util.LogUtil;

public class Conexao {
	
	private String from;
	private String to
	;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		try {
			json.put("from", from);
			json.put("to", to);
			return json;
		} catch (JSONException e) {
			LogUtil.e("Erro ao tentar criar o JsonObject - Conexao...", e);
		}
		
		return null;
	}

}
