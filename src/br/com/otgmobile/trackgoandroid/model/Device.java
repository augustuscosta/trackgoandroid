package br.com.otgmobile.trackgoandroid.model;

import java.io.Serializable;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Device implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 940207071151414120L;

	@DatabaseField(id = true)
	private Long id;
	
	@DatabaseField
	private String code;
	
	@DatabaseField
	private String name;
	
	@DatabaseField
	private String description;
	
	@DatabaseField
	private Boolean enabled;
	
	@DatabaseField(dataType=DataType.ENUM_STRING)
	private DeviceType deviceType;
	
	@DatabaseField(foreign= true,foreignAutoCreate=true, foreignAutoRefresh=true,foreignColumnName="id")
	private TrackerData trackerData;
	
	
	private ForeignCollection<TrackerData> positions;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public DeviceType getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}
	
	public TrackerData getTrackeData() {
		return trackerData;
	}
	
	public void setTrackerData(TrackerData trackerData) {
		this.trackerData = trackerData;
	}
	
	public ForeignCollection<TrackerData> getPositions() {
		return positions;
	}
	
	public void setPositions(ForeignCollection<TrackerData> positions) {
		this.positions = positions;
	}

	public Device(){
		// for use with orm
	}
}
