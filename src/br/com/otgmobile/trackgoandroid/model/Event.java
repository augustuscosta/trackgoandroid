package br.com.otgmobile.trackgoandroid.model;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="events")
public class Event implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -6698134738438121894L;

	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField()
	private String name;
	
	@DatabaseField()
	private String description;
	
	
	@DatabaseField()
	private Boolean enable = true;
	
	@DatabaseField(foreign= true,foreignAutoCreate=true, foreignAutoRefresh=true,foreignColumnName="id")
	private EventHistoryDetail eventHistoryDetail;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	
	public EventHistoryDetail getEventHistoryDetail() {
		return eventHistoryDetail;
	}

	public void setEventHistoryDetail(EventHistoryDetail eventHistoryDetail) {
		this.eventHistoryDetail = eventHistoryDetail;
	}

	
	public Event(){
		// for use with orm
	}


}
