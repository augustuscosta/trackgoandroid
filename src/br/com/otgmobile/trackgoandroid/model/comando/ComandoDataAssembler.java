package br.com.otgmobile.trackgoandroid.model.comando;

import br.com.otgmobile.trackgoandroid.model.Device;

public class ComandoDataAssembler {
	
	private Device device;
	private Boolean activate;
	private Outputs exits;
	private ApnSettings apnSettings;
	private ServerSettings serverSettings;
	private Integer timeZone;
	private CommandTypeEnum commandTypeEnum;

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Boolean isActivate() {
		return activate;
	}

	public void setActivate(Boolean activate) {
		this.activate = activate;
	}

	public Outputs getExits() {
		return exits;
	}

	public void setExits(Outputs exits) {
		this.exits = exits;
	}

	public ApnSettings getApnSettings() {
		return apnSettings;
	}

	public void setApnSettings(ApnSettings apnSettings) {
		this.apnSettings = apnSettings;
	}

	public ServerSettings getServerSettings() {
		return serverSettings;
	}

	public void setServerSettings(ServerSettings serverSettings) {
		this.serverSettings = serverSettings;
	}

	public Integer getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(Integer timeZone) {
		this.timeZone = timeZone;
	}

	public CommandTypeEnum getCommandTypeEnum() {
		return commandTypeEnum;
	}

	public void setCommandTypeEnum(CommandTypeEnum commandTypeEnum) {
		this.commandTypeEnum = commandTypeEnum;
	}

}
