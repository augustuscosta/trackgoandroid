package br.com.otgmobile.trackgoandroid.model.comando;

public class ServerSettings {
	
	private String primaryIp; 
	private String primaryPort; 
	private String seconderyIp; 
	private String seconderyPort;
	
	public String getPrimaryIp() {
		return primaryIp;
	}
	
	public void setPrimaryIp(String primaryIp) {
		this.primaryIp = primaryIp;
	}
	
	public String getPrimaryPort() {
		return primaryPort;
	}
	
	public void setPrimaryPort(String primaryPort) {
		this.primaryPort = primaryPort;
	}
	
	public String getSeconderyIp() {
		return seconderyIp;
	}
	
	public void setSeconderyIp(String seconderyIp) {
		this.seconderyIp = seconderyIp;
	}
	
	public String getSeconderyPort() {
		return seconderyPort;
	}
	
	public void setSeconderyPort(String seconderyPort) {
		this.seconderyPort = seconderyPort;
	}

}
