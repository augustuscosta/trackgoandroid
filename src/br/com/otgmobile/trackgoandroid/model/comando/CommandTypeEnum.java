package br.com.otgmobile.trackgoandroid.model.comando;

public enum CommandTypeEnum {
	
	REQUEST_POSITION(0),
	DISABLE_PANIC(1),
	ANTI_THEFT(2),
	DISABABLE_ANTI_THEFT(3),
	TIME_ZONE(4),
	OUTPUT(5),
	APN(6),
	SERVER(7),
	RESET(8);
	
	private int value;
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	CommandTypeEnum(int value){
		this.value =value;
	}
	
	public static CommandTypeEnum fromValue(int value){
		for(CommandTypeEnum commandTypeEnum: CommandTypeEnum.values()){
			if(commandTypeEnum.getValue() == value) return commandTypeEnum;
		}
		return null;
	}
	
	public static CommandTypeEnum fromName(String name){
		for(CommandTypeEnum commandTypeEnum: CommandTypeEnum.values()){
			if(commandTypeEnum.name().equals(name)) return commandTypeEnum;
		}
		return null;
	}

}
