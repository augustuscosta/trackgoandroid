package br.com.otgmobile.trackgoandroid.model.comando;

public class Outputs {
	
	private Integer exit1;

	private Integer exit2;
	
	private Integer exit3;

	public Integer getExit1() {
		return exit1;
	}

	public void setExit1(Integer exit1) {
		this.exit1 = exit1;
	}

	public Integer getExit2() {
		return exit2;
	}

	public void setExit2(Integer exit2) {
		this.exit2 = exit2;
	}

	public Integer getExit3() {
		return exit3;
	}

	public void setExit3(Integer exit3) {
		this.exit3 = exit3;
	}

}
