package br.com.otgmobile.trackgoandroid.cloud;

import android.content.Context;
import br.com.otgmobile.trackgoandroid.model.comando.ComandoDataAssembler;
import br.com.otgmobile.trackgoandroid.model.comando.CommandTypeEnum;
import br.com.otgmobile.trackgoandroid.util.AppHelper;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;
import br.com.otgmobile.trackgoandroid.util.Session;

public class ComandoCloud extends RestClient {
	
	
	public void sendCommandToServer(Context context,ComandoDataAssembler comando) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		addParam("device", comando.getDevice().getId().toString());
		addParamsFromComando(comando);
		setUrl(addSlashIfNeeded(url) + getURLFromCommandType(comando.getCommandTypeEnum()));
		execute(RequestMethod.POST);
		if(getResponseCode() == ConstUtil.INVALID_TOKEN){
			AppHelper.sendFinishActivityBroadcast(context, getResponseCode());
		}
	} 

	private String getURLFromCommandType(CommandTypeEnum commandTypeEnum) {
		return ComandoURLEnum.fromName(commandTypeEnum.name()).getUrl();
	}

	private void addParamsFromComando(ComandoDataAssembler comando) {
		switch (comando.getCommandTypeEnum()) {
			
		case TIME_ZONE:
			addParam("timeZone", comando.getTimeZone().toString());
			break;
			
		case OUTPUT:
			addParam("output1", comando.getExits().getExit1().toString() );
			addParam("output2", comando.getExits().getExit2().toString() );			
			addParam("output3", comando.getExits().getExit3().toString() );			
			
			break;
			
		case APN:
			addParam("url", comando.getApnSettings().getUrl());
			addParam("username", comando.getApnSettings().getUsername());
			addParam("password", comando.getApnSettings().getPassword());
			
			break;
			
		case SERVER:
			
			break;
			
			default:
				break;

		}
		
	}

}
