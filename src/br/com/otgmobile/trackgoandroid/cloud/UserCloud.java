package br.com.otgmobile.trackgoandroid.cloud;

import org.apache.http.HttpStatus;

import android.content.Context;
import android.content.Intent;
import br.com.otgmobile.trackgoandroid.model.User;
import br.com.otgmobile.trackgoandroid.util.BroadCastDictionary;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;
import br.com.otgmobile.trackgoandroid.util.Session;

public class UserCloud extends RestClient {
	
	private static final String URL = "mobile/session";
	
	
	public void login(Context context, User user) throws Throwable{
		cleanParams();
		String url = Session.getServer(context);
		addParam("username", user.getNome());
		addParam("password", user.getSenha());
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.POST);
		handleLoginResult(context);
		
	}


	private void handleLoginResult(Context context) {
		switch (getResponseCode()) {
		case HttpStatus.SC_OK:
			saveToken(getResponse(), context);
			break;
			
		case ConstUtil.INVALID_TOKEN:
			Intent intent = new Intent();
			intent.setAction(BroadCastDictionary.INVALID_TOKEN.getValue());
			context.sendBroadcast(intent);
			break;

		default:
			break;
		}
		
	}


	private void saveToken(String tokenFromResponse,Context context) {
		Session.setToken(tokenFromResponse, context);
		
	}



}
