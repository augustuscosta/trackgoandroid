package br.com.otgmobile.trackgoandroid.cloud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RestClient {
	
	public enum RequestMethod{
	    GET,
	    POST,
	    PUT,
	    DELETE
	}
	 
    protected ArrayList <NameValuePair> params;
    protected ArrayList <NameValuePair> headers;
 
    protected String url;
 
    protected int responseCode;
    protected String message;
 
    protected String response;
    
    public void cleanParams(){
    	params = new ArrayList<NameValuePair>();
        headers = new ArrayList<NameValuePair>();
    }
 
    public String getResponse() {
        return response;
    }
 
    public String getErrorMessage() {
        return message;
    }
 
    public int getResponseCode() {
        return responseCode;
    }
    
    public boolean isWrongUserOrPasswordCause(){
    	if(response != null){
    		if(response.lastIndexOf("%WRONG_USER_OR_PASSWORD%") > 0){
    			return true;
    		}
    	}
    	return false;
    }
    public boolean isInvalidTokenCause(){
    	if(response != null){
    		if(response.lastIndexOf("%INVALID_TOKEN%") > 0){
    			return true;
    		}
    	}
    	return false;
    }
    
    public boolean isInvalidDeviceCause(){
    	if(response != null){
    		if(response.lastIndexOf("%INVALID_DEVICE%") > 0){
    			return true;
    		}
    	}
    	return false;
    }
    
    public String addSlashIfNeeded(String url){
    	if(url.lastIndexOf("/") != url.length() -1){
    		return url + "/";
    	}
    	return url;
    }
 
    public void addParam(String name, String value)
    {
        params.add(new BasicNameValuePair(name, value));
    }
 
    public void addHeader(String name, String value)
    {
        headers.add(new BasicNameValuePair(name, value));
    }
 
    public void execute(RequestMethod method) throws Exception
    {
        switch(method) {
            case GET:
            {
            	String combinedParams = getUrlParams();
                
                String urlWithParams = url + combinedParams;
 
                HttpGet request = new HttpGet(urlWithParams);
 
                getHeaderParams(request);
 
                executeRequest(request, urlWithParams);
                break;
            }
            case POST:
            {
                HttpPost request = new HttpPost(url);
 
                getHeaderParams(request);
 
                if(!params.isEmpty()){
                    request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                }
 
                executeRequest(request, url);
                break;
            }
            case PUT:
            {
            	String combinedParams = getPutParams();
                
                String urlWithParams = url + combinedParams;
 
                HttpPut request = new HttpPut(urlWithParams);
 
                getHeaderParams(request);
 
                executeRequest(request, urlWithParams);
                break;
            }
            case DELETE:
            {
            	String combinedParams = getUrlParams();
                
                String urlWithParams = url + combinedParams;
 
                HttpDelete request = new HttpDelete(urlWithParams);
 
                getHeaderParams(request);
 
                executeRequest(request, urlWithParams);
                break;
            }
        }
    }

	private String getPutParams() throws UnsupportedEncodingException {
		String combinedParams = "?";
		if(!params.isEmpty()){
		    for(NameValuePair p : params)
		    {
		    	if(combinedParams.lastIndexOf("?") != combinedParams.length() - 1){
		    		combinedParams  += "&";
		    	}
		        String paramString = URLEncoder.encode(p.getValue(),"UTF-8");
		        String keyString = URLEncoder.encode(p.getName(),"UTF-8");
		        combinedParams  += keyString + "=" + paramString;
		    }
		}
		return combinedParams;
	}

	private void getHeaderParams(HttpRequest request) {
		for(NameValuePair h : headers)
		{
		    request.addHeader(h.getName(), h.getValue());
		}
	}

	private String getUrlParams() throws UnsupportedEncodingException {
		String combinedParams = "";
		if(!params.isEmpty()){
		    for(NameValuePair p : params)
		    {
		        String paramString = URLEncoder.encode(p.getValue(),"UTF-8");
		        combinedParams  += "/" + paramString;
		    }
		}
		return combinedParams;
	}
    
    public JSONArray getJsonObjectArrayFromResponse(String rootObjectName) throws JSONException{
    	if(response == null){
    		return null;
    	}
    	JSONObject jObject = new JSONObject(response);
    	return jObject.getJSONArray(rootObjectName);
    }
    
    public JSONObject getJsonObjectFromResponse(String rootObjectName) throws JSONException{
    	if(response == null){
    		return null;
    	}
    	JSONObject jObject = new JSONObject(response);
    	return jObject.getJSONObject(rootObjectName);
    }
    
    public JSONObject getJsonObjectFromResponse() throws JSONException{
    	if(response == null){
    		return null;
    	}
    	JSONObject jObject = new JSONObject(response);
    	return jObject;
    }
 
    protected void executeRequest(HttpUriRequest request, String url)
    {
        HttpClient client = new DefaultHttpClient();
 
        HttpResponse httpResponse;
        InputStream instream;
        
        try {
            httpResponse = client.execute(request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();
 
            HttpEntity entity = httpResponse.getEntity();
 
            if (entity != null) {
 
                instream = entity.getContent();
                response = convertStreamToString(instream);
                instream.close();
            }
 
        } catch (ClientProtocolException e)  {
            client.getConnectionManager().shutdown();
            message = e.getLocalizedMessage();
        } catch (IOException e) {
            client.getConnectionManager().shutdown();
            message = e.getLocalizedMessage();
        }
    }
    
 
    protected static String convertStreamToString(InputStream is) {
 
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

	public ArrayList<NameValuePair> getParams() {
		return params;
	}

	public void setParams(ArrayList<NameValuePair> params) {
		this.params = params;
	}

	public ArrayList<NameValuePair> getHeaders() {
		return headers;
	}

	public void setHeaders(ArrayList<NameValuePair> headers) {
		this.headers = headers;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public void setResponse(String response) {
		this.response = response;
	}
    
    
}
