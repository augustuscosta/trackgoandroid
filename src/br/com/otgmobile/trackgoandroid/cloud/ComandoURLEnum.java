package br.com.otgmobile.trackgoandroid.cloud;

public enum ComandoURLEnum {
	
	REQUEST_POSITION("mobile/comando/request_position"),
	DISABLE_PANIC("mobile/comando/disable_panic"),
	ANTI_THEFT("mobile/comando/anti_theft"),
	DISABABLE_ANTI_THEFT("mobile/comando/disable_anti_theft"),
	TIME_ZONE("mobile/comando/time_zone"),
	OUTPUT("mobile/comando/output"),
	APN("mobile/comando/apn"),
	SERVER("mobile/comando/server"),
	RESET("mobile/comando/reset");
	
	private String url;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	ComandoURLEnum(String url){
		this.url = url;
	}
	
	public static ComandoURLEnum fromName(String name){
		for (ComandoURLEnum comandoURLEnum : ComandoURLEnum.values()) {
			if(name.equals(comandoURLEnum.name())) return comandoURLEnum;
		}
		
		return null;
	}
	

}
