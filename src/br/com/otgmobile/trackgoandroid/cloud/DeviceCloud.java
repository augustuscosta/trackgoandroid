package br.com.otgmobile.trackgoandroid.cloud;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.json.JSONArray;

import android.content.Context;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.util.AdvancedJsonUtils;
import br.com.otgmobile.trackgoandroid.util.AppHelper;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;
import br.com.otgmobile.trackgoandroid.util.LogUtil;
import br.com.otgmobile.trackgoandroid.util.Session;

public class DeviceCloud extends RestClient{
	
	private final static String URL = "mobile/vehicles";
	private final static String ROOT_OBJECT = "devices";
	
	public List<Device> getDeviceFromServer(Context context) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		if(getResponseCode() == HttpStatus.SC_OK){
			return getListFromResponse();
		}
		LogUtil.e(getErrorMessage());
		if(getResponseCode() == ConstUtil.INVALID_TOKEN){
			AppHelper.sendFinishActivityBroadcast(context, getResponseCode());
		}
		return null;
	}

	private List<Device> getListFromResponse() throws Exception {
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if (jsonArray != null) {
			Device obj;
			List<Device> toReturn = new ArrayList<Device>();
			for (int i = 0; i < jsonArray.length(); i++) {

				obj = AdvancedJsonUtils.returnDeviceWithTrackerDataFromJson(jsonArray.getJSONObject(i));
				toReturn.add(obj);
			}
			return toReturn;
		}
		return null;
	}
	
	public static void storeResponse(){
		
	}

}
