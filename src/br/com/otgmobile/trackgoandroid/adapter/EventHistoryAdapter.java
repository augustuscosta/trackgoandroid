package br.com.otgmobile.trackgoandroid.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.row.EventHistoryRowHolderInfo;

public class EventHistoryAdapter extends BaseAdapter {
	
	private List<EventHistoryDetail> events;
	private Activity context;
	
	public EventHistoryAdapter(List<EventHistoryDetail> events, Activity context){
		this.events = events;
		this.context = context;
	}

	public int getCount() {
		return events.size();
	}

	public Object getItem(int position) {
		return events.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		EventHistoryRowHolderInfo rowInfo;
		EventHistoryDetail obj = events.get(position);
		
		if(view == null){
			rowInfo = new EventHistoryRowHolderInfo();
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.event_row, null);
			rowInfo.dateTextView = (TextView) row.findViewById(R.id.eventListDate);
			rowInfo.deviceTextView = (TextView) row.findViewById(R.id.eventListdeviceCode);
			rowInfo.eventDescriptionTextView = (TextView) row.findViewById(R.id.eventListdecription);
			row.setTag(rowInfo);

		}else{
			rowInfo = (EventHistoryRowHolderInfo) view.getTag();
		}
		
		rowInfo.drawRow(obj);
		return row;
	}

}
