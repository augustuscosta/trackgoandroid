package br.com.otgmobile.trackgoandroid.socket.impl;

import java.sql.SQLException;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import br.com.otgmobile.trackgoandroid.database.DatabaseHelper;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.model.EventOcurred;
import br.com.otgmobile.trackgoandroid.util.AdvancedJsonUtils;
import br.com.otgmobile.trackgoandroid.util.BroadCastDictionary;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;
import br.com.otgmobile.trackgoandroid.util.LogUtil;
import br.com.otgmobile.trackgoandroid.util.NotificationUtil;

import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;


public class SocketManagerDelegate {

	private static final String ROOT_JSON_OBJECT = "result";

	private Context context;

	private Dao<Device, Long> dao;

	public SocketManagerDelegate(Context context) {
		this.context = context;
	}

	public void delegateEvent(final String event, final JSONObject... data) {
		if ( (event == null || event.trim().length() == 0) || !isExistData(data) ) {
			return;
		}
		LogUtil.i("NODE EVENT: " + event);
		LogUtil.i("Data: " + data);
		Intent intent = new Intent();
		final EventType eventType = EventType.toSocketRequestType(event);
		switch (eventType) {

		case EVENT_HISTORY:
			EventHistoryDetail eventHistory;
			try {
				Gson gson = new Gson();
				JSONObject json = AdvancedJsonUtils.getJsonObjectFromSocket(ROOT_JSON_OBJECT, data[0].toString());
				EventOcurred eventOcurred = gson.fromJson(json.toString(), EventOcurred.class);
				eventHistory = eventOcurred.assembleEventData();
				verifyAndNotify(intent, eventHistory);
			} catch (Exception e) {
				LogUtil.e("erro ao pegar json do socket",e);
			}
			
			break;

		default:
			
			break;
		
		}
		

	}

	private void verifyAndNotify(Intent intent, EventHistoryDetail eventHistory)throws SQLException {
		String action;
		Long id = eventHistory.getDevice().getId();
		Device device  =dao().queryForId(id);
		if(device == null)  return;
		action = BroadCastDictionary.NEW_EVENT.getValue();
		Bundle b = new Bundle();
		b.putSerializable(ConstUtil.SERIALIZABLE_KEY, eventHistory);
		intent.putExtras(b);
		intent.setAction(action);
		context.sendBroadcast(intent);
		NotificationUtil.createNewOcurenceNotification(eventHistory, context);
	}

	private boolean isExistData(JSONObject[] data) {
		return data != null && data.length > 0;
	}
	
	private Dao<Device,Long> dao(){
		if(dao == null){
			try {
				dao =  DatabaseHelper.getDatabase(context).getDao(Device.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar dao", e);
			}
		}
		
		return dao;
	}

}
