package br.com.otgmobile.trackgoandroid.socket.impl;

import android.util.Log;

public enum EventType {
	
	ATUALIZA_SESSAO("sessao/token"),
	ENVIA_DEVICES("events/devices"),
	EVENT_HISTORY("events/event_history");

	
	;

	private String value;

	private EventType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}

	public static EventType toSocketRequestType(String value) {
		final EventType[] requestTypes = EventType.values();
		for (EventType type : requestTypes) {
			if ( type.getValue().equalsIgnoreCase(value) ) {
				return type;
			}
		}
		
		Log.d(EventType.class.getSimpleName(), "Does not exist request type = " + value);
		return null;
	}
	
}
