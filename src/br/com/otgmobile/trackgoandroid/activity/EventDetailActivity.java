package br.com.otgmobile.trackgoandroid.activity;

import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.database.DatabaseHelper;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.Event;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;

public class EventDetailActivity extends GenericActivity<DatabaseHelper> {
	
	
	@InjectView(R.id.event_detail_container) LinearLayout eventLinearLayout;
	@InjectView(R.id.event_name_text) TextView eventTextView;
	@InjectView(R.id.description_text) TextView eventDescriptionTextView;
	@InjectView(R.id.device_name_text) TextView deviceNameTextView;
	@InjectView(R.id.tracker_text) TextView trackeTextView;
	@InjectView(R.id.tracker_type_text) TextView trackeTypeTextView;
	@InjectView(R.id.latitude_text) TextView latitudeText;
	@InjectView(R.id.longitude_text) TextView longitudeText;
	@InjectView(R.id.date_text) TextView dateText;
	
	private EventHistoryDetail eventHistoryDetail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_detail);
		handleIntent();
	}

	private void handleIntent() {
		Intent intent = getIntent();
		if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
			eventHistoryDetail = (EventHistoryDetail) intent.getSerializableExtra(ConstUtil.SERIALIZABLE_KEY);
		}if(eventHistoryDetail!= null){
			setDataOnView();
		}
	}

	private void setDataOnView() {
		Device device = eventHistoryDetail.getDevice();
		Event event = eventHistoryDetail.getEvent();
		
		setRawEventHistoryDetailOnView();
		setDeviceDetailsOnView(device);
		setEventDetailsOnView(event);
	}

	private void setRawEventHistoryDetailOnView() {
		setLatitudeAndLongitudeOnView();
		setDateOnView();
	}
	
	private void setLatitudeAndLongitudeOnView() {
		if(eventHistoryDetail.getLatitude()!= null){
			latitudeText.setText(eventHistoryDetail.getLatitude().toString());
		}else{
			latitudeText.setText(getString(R.string.no_data));;				
		}
		
		if(eventHistoryDetail.getLongitude()!= null){
			longitudeText.setText(eventHistoryDetail.getLongitude().toString());
		}else{
			longitudeText.setText(getString(R.string.no_data));;				
		}
	}
	
	private void setDateOnView() {
		if(eventHistoryDetail.getMetricDate() != null){
			dateText.setText(eventHistoryDetail.getMetricDate().toLocaleString());
		}else{
			dateText.setText(getString(R.string.no_data));;				
		}
	}
	
	private void setDeviceDetailsOnView(Device device) {
		if(device == null) return;
		if(device.getName() != null){
			deviceNameTextView.setText(device.getName());
		}else{
			deviceNameTextView.setText(getString(R.string.no_data));
		}
		
		if(device.getCode() != null){
			trackeTextView.setText(device.getCode());
		}else{
			trackeTextView.setText(getString(R.string.no_data));
		}
		
		if(device.getDeviceType() != null){
			trackeTypeTextView.setText(device.getDeviceType().name());
		}else{
			trackeTypeTextView.setText(getString(R.string.no_data));
		}
		
	}

	private void setEventDetailsOnView(Event event) {
		if(event == null) return;
		if(event.getName() != null){
			eventTextView.setText(event.getName());
		}else{
			eventTextView.setText(getString(R.string.no_data));
		}
		
		if(event.getDescription() != null){
			eventDescriptionTextView.setText(event.getDescription());
		}else{
			eventDescriptionTextView.setText(getString(R.string.no_data));
		}
		
	}
	
	public void onClickSendComand(View view){
			if(eventHistoryDetail!= null && eventHistoryDetail.getDevice() != null){
				Intent intent = new Intent(EventDetailActivity.this,ComandoActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, eventHistoryDetail.getDevice());
				intent.putExtras(bundle);
				startActivityForResult(intent, ConstUtil.COMANDO);
			}
		
	}


}
