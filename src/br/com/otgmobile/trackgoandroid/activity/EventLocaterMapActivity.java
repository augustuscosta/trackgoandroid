package br.com.otgmobile.trackgoandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.database.DatabaseHelper;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class EventLocaterMapActivity extends GenericFragmentActivity<DatabaseHelper> implements OnInfoWindowClickListener{

	private EventHistoryDetail eventHistoryDetail;



	private GoogleMap map;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_map);
		initMap();
		handleIntent();
	}
	
	private void initMap(){
		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        map = fm.getMap();
        map.setMyLocationEnabled(true);
        map.setOnInfoWindowClickListener(this);
	}


	private void handleIntent() {
		Intent intent = getIntent();
		if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
			eventHistoryDetail = (EventHistoryDetail) intent.getSerializableExtra(ConstUtil.SERIALIZABLE_KEY);
		}
		if(eventHistoryDetail != null){
			cleanMap();
			setEventOnMap(eventHistoryDetail);
		}

	}


	private void setEventOnMap(EventHistoryDetail eventHistoryDetail) {
		map.addMarker(getMarker(eventHistoryDetail));
		LatLng position = new LatLng(eventHistoryDetail.getLatitude(), eventHistoryDetail.getLongitude());
		centerMap(position);
	}


	private MarkerOptions getMarker(EventHistoryDetail eventHistoryDetail) {
		String titulo = eventHistoryDetail.getEvent().getName();
		String descricao = eventHistoryDetail.getEvent().getDescription();

		
		LatLng position = new LatLng(eventHistoryDetail.getLatitude(), eventHistoryDetail.getLongitude());
		
		MarkerOptions marker = new MarkerOptions()
        .position(position)
        .title(titulo)
        .snippet(descricao)
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ocorrencia_vermelho));
		return marker;
	}



	private void cleanMap(){
		map.clear();
	}




	public void eventSelected(EventHistoryDetail event) {
		Intent intent = new Intent(EventLocaterMapActivity.this,EventDetailActivity.class);
		Bundle extras = new Bundle();
		extras.putSerializable(ConstUtil.SERIALIZABLE_KEY, event);
		intent.putExtras(extras);
		startActivityForResult(intent, ConstUtil.EVENT_DETAIL);

	}

	public void onInfoWindowClick(Marker marker) {
		eventSelected(eventHistoryDetail);
	}
	
	private void centerMap(LatLng latLng) {
		map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(15));
	}

}
