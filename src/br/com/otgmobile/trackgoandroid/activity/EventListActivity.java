package br.com.otgmobile.trackgoandroid.activity;

import java.sql.SQLException;
import java.util.List;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.adapter.EventHistoryAdapter;
import br.com.otgmobile.trackgoandroid.cloud.EventCloud;
import br.com.otgmobile.trackgoandroid.database.DatabaseHelper;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.Event;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.model.TrackerData;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;
import br.com.otgmobile.trackgoandroid.util.LogUtil;
import br.com.otgmobile.trackgoandroid.util.Session;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

public class EventListActivity extends GenericListActivity<DatabaseHelper>{

	@InjectView(R.id.event_list_container) LinearLayout eventLinearLayout;

	private static GetEventsAsyncTask getEventsAssyncTask;
	private Dao<EventHistoryDetail, Integer> dao;
	private Dao<Event, Integer> eventDao;
	private List<EventHistoryDetail> events;
	private static EventHistoryAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_list);
		initializeDAO();
		getFromCloud();
	}

	private void initializeDAO() {
		try {
			dao = getHelper().getDao(EventHistoryDetail.class);
			eventDao = getHelper().getDao(Event.class);
		} catch (SQLException e) {
			LogUtil.e("erro ao instanciar dao", e);
		}
	}

	@Override
	public void onBackPressed() {
		showLogOffMessage();
	};

	private void showLogOffMessage() {
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_info)
		.setTitle(R.string.log_off_title)
		.setMessage(R.string.log_off)
		.setCancelable(false)
		.setNegativeButton(R.string.no,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
				dialog.cancel();
			}
		}).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
				clearSession();
			}
		}).show();

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		EventHistoryDetail event = events.get(position);
		Intent intent = new Intent(EventListActivity.this, EventLocaterMapActivity.class);
		Bundle b = new Bundle();
		b.putSerializable(ConstUtil.SERIALIZABLE_KEY, event);
		intent.putExtras(b);
		startActivityForResult(intent, ConstUtil.EVENT_MAP);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.map_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_logout:
			clearSession();
			break;

		case R.id.refresh:
			getFromCloud();
			break;
		}

		return true;
	}

	private void clearSession() {
		try {
			dao.deleteBuilder().delete();
			getHelper().getDao(TrackerData.class).deleteBuilder().delete();
			getHelper().getDao(Device.class).deleteBuilder().delete();
		} catch (SQLException e) {
			LogUtil.e("erro ao deletar os dados",e);
		}
		Session.clearSession(this);
		finish();
	}

	private void getFromCloud() {
		stopAsyncTask();
		startAsyncTask();

	}
	private void startAsyncTask() {
		if(getEventsAssyncTask == null){
			getEventsAssyncTask = new GetEventsAsyncTask(this);
		}

		getEventsAssyncTask.execute();
	}

	private void stopAsyncTask() {
		if(getEventsAssyncTask != null){
			getEventsAssyncTask.cancel(true);
			getEventsAssyncTask = null;
		}
	}

	private class GetEventsAsyncTask extends AsyncTask<Void, Void, List<EventHistoryDetail>>{

		protected  ProgressDialog dialog;
		private Context context;

		protected GetEventsAsyncTask(Context context) {
			super();
			this.context = context;
			dialog =ProgressDialog.show(context, "", getString(R.string.processing), true, false);
			dialog.setCancelable(true);
			dialog.setOnCancelListener(eventAsyncTaskCancelListener);
		}


		@Override
		protected List<EventHistoryDetail> doInBackground(Void... params) {
			List<EventHistoryDetail> events = null;
			try {
				events = new EventCloud().getEventsFromServer(context);
			} catch (Exception e) {
				LogUtil.e("erro ao pegar eventos das nuvens", e);
			}
			if(events == null || events.isEmpty()) return null;
			try{
				LogUtil.i("recebidos "+events.size()+" das nuvens");
				for(EventHistoryDetail eventHistory :events){
					LogUtil.i("persistindo eventos");
					if(eventHistory.getEvent() != null){
						eventDao.createOrUpdate(eventHistory.getEvent());
					}
					dao.createOrUpdate(eventHistory);
				}				
				populate();
			}catch(Exception e){
				LogUtil.e("erro ao persistir eventos das nuvens", e);
			}
			return events;
		}

		@Override
		protected void onPostExecute(List<EventHistoryDetail> t) {
			if(dialog != null || dialog.isShowing()){
				dialog.dismiss();
			}
			populate();
			

		}
	}

	private void populate() {
		try {
			QueryBuilder<EventHistoryDetail,Integer> queryBuilder = dao.queryBuilder();
			Long limit = (long)100;
			events = queryBuilder.limit(limit).query();
			setAdapter();

		} catch (SQLException e) {
			LogUtil.e("Erro ao pegar lista de eventos do banco", e);
		}finally{
			try {
				dao.closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar o ultimo iterador do banco",e);
			}
		}
	}

	private void setAdapter() {
		adapter = new EventHistoryAdapter(events, this);
		setListAdapter(adapter);
	}


	private OnCancelListener  eventAsyncTaskCancelListener = new OnCancelListener() {

		public void onCancel(DialogInterface arg0) {
			if(getEventsAssyncTask != null) getEventsAssyncTask.cancel(true);

		}
	}; 


}
