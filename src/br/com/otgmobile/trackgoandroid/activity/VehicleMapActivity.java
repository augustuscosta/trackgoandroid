package br.com.otgmobile.trackgoandroid.activity;

import java.sql.SQLException;
import java.util.List;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.cloud.DeviceCloud;
import br.com.otgmobile.trackgoandroid.database.DatabaseHelper;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.model.TrackerData;
import br.com.otgmobile.trackgoandroid.util.BroadCastDictionary;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;
import br.com.otgmobile.trackgoandroid.util.LogUtil;
import br.com.otgmobile.trackgoandroid.util.Session;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

public class VehicleMapActivity extends GenericFragmentActivity<DatabaseHelper> implements OnInfoWindowClickListener{

	private static GetDevicesAsyncTask getDevicesAssyncTask;
	private List<Device> devices;
	private Dao<Device,Integer> dao;
	private Dao<TrackerData,Integer> trackerDao;
	private String filterText ="";


	private GoogleMap map;
	
	@InjectView(R.id.filterTextView) TextView filterTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.veheicle_map);
		filterTextView = (TextView) findViewById(R.id.filterTextView);
		addTextWatcherToFilter();
		initMap();
		initializeDAOS();
		getFromCloud();
	}
	
	private void initMap(){
		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        map = fm.getMap();
        map.setMyLocationEnabled(true);
        map.setOnInfoWindowClickListener(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}


	private void addTextWatcherToFilter() {
		filterTextView.addTextChangedListener(filterTextWatcher);
	}


	@Override
	public void onBackPressed() {
		showLogOffMessage();
	};

	private void showLogOffMessage() {
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_info)
		.setTitle(R.string.log_off_title)
		.setMessage(R.string.log_off)
		.setCancelable(false)
		.setNegativeButton(R.string.no,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
				dialog.cancel();
			}
		}).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
				clearSession();
			}
		}).show();
		
	}


	private void initializeDAOS() {
		try {
			trackerDao = getHelper().getDao(TrackerData.class);
			dao = getHelper().getDao(Device.class);
		} catch (SQLException e) {
			LogUtil.e("erro ao criar dao de device", e);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.map_menu, menu);
		return true;
	}

	private void cleanMap(){
		map.clear();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_logout:
			clearSession();
			break;

		case R.id.refresh:
			getFromCloud();
			break;
		}

		return true;
	}

	private void clearSession() {
		try {
			dao.deleteBuilder().delete();
			trackerDao.deleteBuilder().delete();
			getHelper().getDao(EventHistoryDetail.class).deleteBuilder().delete();
		} catch (SQLException e) {
			LogUtil.e("erro ao deletar os dados",e);
		}
		Session.clearSession(this);
		finish();
	}

	private void populate() {
			queryForFilter(filterText);
			addMarkersToMap();
	}

	private void getFromCloud(){
		startDevicesAsyncTask();
	}

	private void startDevicesAsyncTask() {
		stopDeviceAsyncTask();
		getDevicesAssyncTask = new GetDevicesAsyncTask(this);
		getDevicesAssyncTask.execute();

	}

	private void stopDeviceAsyncTask() {
		if(getDevicesAssyncTask != null){
			getDevicesAssyncTask.cancel(true);
			getDevicesAssyncTask = null;
		}

	}

	class GetDevicesAsyncTask extends AsyncTask<Void,Void,List<Device>> {

		protected  ProgressDialog dialog;
		private Context context;

		protected GetDevicesAsyncTask(Context context) {
			this.context = context;
			dialog =ProgressDialog.show(context, "", getString(R.string.processing), true, false);
			dialog.setCancelable(true);
		}

		@Override
		protected void onPreExecute() {
			dialog.setMessage(context.getString(R.string.processing));
			dialog.show();
			dialog.setOnCancelListener(deviceAsyncTaskCancelListener);
		}

		@Override
		protected List<Device> doInBackground(Void... arg0) {
			DeviceCloud cloud = new DeviceCloud();
			List<Device> devices = null;
			try {
				devices = cloud.getDeviceFromServer(context);
				if(devices == null || devices .isEmpty()) return null;

				for(Device device :devices){
					if(device.getTrackeData()!=null){
						trackerDao.createOrUpdate(device.getTrackeData());
					}
					dao.createOrUpdate(device);				

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 	devices;
		}
		
		@Override
		protected void onPostExecute(List<Device> result) {
				if (dialog.isShowing()) {
					dialog.cancel();
					dialog = null;
				}
				sendBroadstToService();
				populate();
				super.onPostExecute(result);
			}
	}


	private void addMarkersToMap() {
		cleanMap();
		addDevicesToMap();
	}

	private void addDevicesToMap() {
		if(devices == null || devices.isEmpty()) return;

		Device toCenter = null;
		for (Device device : devices) {
			if(device.getTrackeData() != null){
				map.addMarker(getDeviceMarker(device));
				toCenter = device;
			}
		}
		if(toCenter != null){
			LatLng position = new LatLng(toCenter.getTrackeData().getLatitude(), toCenter.getTrackeData().getLongitude());
			centerMap(position);
		}
	}
	
	private void centerMap(LatLng latLng) {
		map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(15));
	}

	private MarkerOptions getDeviceMarker(Device device) {
		String titulo = "";
		String descricao = "";
		if(device.getCode()!= null && device.getCode().length() >0){
			descricao = device.getCode();
		}

		if(device.getName()!= null && device.getName().length() >0){
			titulo = device.getName();
		}
		
		LatLng position = new LatLng(device.getTrackeData().getLatitude(), device.getTrackeData().getLongitude());
		
		MarkerOptions marker = new MarkerOptions()
        .position(position)
        .title(titulo)
        .snippet(descricao)
        .icon(BitmapDescriptorFactory.fromResource(getDeviceDrawable(device)));
		return marker;
	}

	private int getDeviceDrawable(Device device) {
		if(device.getTrackeData().getPanic() == null || device.getTrackeData().getPanic()== false){
			return R.drawable.veiculo_verde;
		}
		return R.drawable.veiculo_vermelho;

	}



	public void deviceSelected(Device device) {
		viewDeviceDetails(device);
	}

	private void viewDeviceDetails(Device device) {
		Intent intent = new Intent(VehicleMapActivity.this,DeviceDetailActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, device);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.DEVICE_DETAIL);
	}

	private OnCancelListener  deviceAsyncTaskCancelListener = new OnCancelListener() {

		public void onCancel(DialogInterface arg0) {
			if(getDevicesAssyncTask != null){
				getDevicesAssyncTask.cancel(true);
				populate();
			}

		}
	}; 

	TextWatcher filterTextWatcher = new TextWatcher() {
		
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			filterText = s.toString();
			queryForFilter(filterText);
		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}
	};
	
	
	private void queryForFilter(String filterText) {
		QueryBuilder<Device, Integer> builder = dao.queryBuilder();
		try {
			builder.where().like("name", "%"+ filterText+"%")
			.or().like("code", "%"+ filterText+"%").or().like("description", "%"+ filterText+"%");
			devices = builder.query();
			addMarkersToMap();
		} catch (SQLException e) {
			LogUtil.e("erro ao extrair os dadeos",e);
		}
	}
	
	private void sendBroadstToService(){
		Intent intent = new Intent();
		String action = BroadCastDictionary.SEND_DEVICE.getValue();
		intent.setAction(action);
		this.sendBroadcast(intent);
	}

	public void searchButtonOnClick(View view){
		queryForFilter(filterTextView.getText().toString());
	}
	
	private Device getDeviceByCodigo(String code){
		if(devices != null){
			for(Device device:devices){
				if(code.equals(device.getCode()))
					return device;
			}
		}
		return null;
	}

	public void onInfoWindowClick(Marker marker) {
		Device device = getDeviceByCodigo(marker.getSnippet());
		deviceSelected(device);
	}
		
}
