package br.com.otgmobile.trackgoandroid.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.util.LogUtil;

public class SplashScreen extends Activity {
	
	protected boolean spashRunning = true;
	protected int splashRunTime = 5000;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);

		Thread splashScreenTread = new Thread() {
			@Override
			public void run() {
				try {
					int waited = 0;
					while (spashRunning && (waited < splashRunTime)) {
						sleep(100);
						if (spashRunning) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
					LogUtil.e("erro ao fechar splashScreen", e);
					
				} finally {
					finish();
				}
			}
		};
		splashScreenTread.start();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			spashRunning = false;
		}
		return true;
	}

}
