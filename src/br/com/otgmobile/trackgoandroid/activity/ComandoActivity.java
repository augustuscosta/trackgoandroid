package br.com.otgmobile.trackgoandroid.activity;

import org.apache.http.HttpStatus;

import roboguice.inject.InjectView;
import roboguice.util.RoboAsyncTask;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.cloud.ComandoCloud;
import br.com.otgmobile.trackgoandroid.cloud.RestClient;
import br.com.otgmobile.trackgoandroid.database.DatabaseHelper;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.TimeZone;
import br.com.otgmobile.trackgoandroid.model.comando.ApnSettings;
import br.com.otgmobile.trackgoandroid.model.comando.ComandoDataAssembler;
import br.com.otgmobile.trackgoandroid.model.comando.CommandTypeEnum;
import br.com.otgmobile.trackgoandroid.model.comando.Outputs;
import br.com.otgmobile.trackgoandroid.model.comando.ServerSettings;
import br.com.otgmobile.trackgoandroid.util.AppHelper;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;

public class ComandoActivity extends GenericActivity<DatabaseHelper> {

	@InjectView(R.id.device_name_text) TextView nameText;
	@InjectView(R.id.device_description_text) TextView decriptionText;
	@InjectView(R.id.device_code_text) TextView codeText;
	@InjectView(R.id.device_tracker_type_text) TextView trackerTypeText;
	@InjectView(R.id.comandoselector) Spinner comandoSelector;

	//Time zone views
	@InjectView(R.id.timeZoneSelector) Spinner timeZoneSelector;
	@InjectView(R.id.timeZoneConteiner) LinearLayout timeZoneConteiner;
	@InjectView(R.id.timeZoneBaseLine) View timeZoneBaseLine;

	//Anti theft views
	@InjectView(R.id.antiTheftBox) ToggleButton antiTheftBox;
	@InjectView(R.id.antiTheftConteiner) LinearLayout antiTheftConteiner;
	@InjectView(R.id.antiTheftBaseLine) View antiTheftBaseline;

	//Outputs views
	@InjectView(R.id.saidaBox1) ToggleButton saida1Box;
	@InjectView(R.id.saida1Conteiner) LinearLayout saida1Conteiner;
	@InjectView(R.id.saida1BaseLine) View saida1Baseline;
	@InjectView(R.id.saidaBox2) ToggleButton saida2Box;
	@InjectView(R.id.saida2Conteiner) LinearLayout saida2Conteiner;
	@InjectView(R.id.saida2BaseLine) View saida2Baseline;
	@InjectView(R.id.saidaBox3) ToggleButton saida3Box;
	@InjectView(R.id.saida3Conteiner) LinearLayout saida3Conteiner;
	@InjectView(R.id.saida3BaseLine) View saida3Baseline;

	//APN Views
	@InjectView(R.id.urlText) TextView urlText;
	@InjectView(R.id.urlConteiner) LinearLayout urlConteiner;
	@InjectView(R.id.urlBaseLine) View urlBaseline;
	@InjectView(R.id.userText) TextView userText;
	@InjectView(R.id.userContainer) LinearLayout userConteiner;
	@InjectView(R.id.userBaseLine) View userBaseline;
	@InjectView(R.id.passText) TextView passText;
	@InjectView(R.id.passTextContainer) LinearLayout passConteiner;
	@InjectView(R.id.passTextBaseLine) View passBaseline;

	// Server Views

	@InjectView(R.id.server1Text) TextView server1Text;
	@InjectView(R.id.ServerTextContainer) LinearLayout server1Conteiner;
	@InjectView(R.id.serverTextBaseLine) View server1Baseline;
	@InjectView(R.id.port1Text) TextView portText;
	@InjectView(R.id.portTextContainer) LinearLayout portConteiner;
	@InjectView(R.id.portTextBaseLine) View portBaseline;
	@InjectView(R.id.server2Text) TextView server2Text;
	@InjectView(R.id.Server2TextContainer) LinearLayout server2Container;
	@InjectView(R.id.Server2TextBaseLine) View server2Baseline;
	@InjectView(R.id.port2Text) TextView port2Text;
	@InjectView(R.id.port2TextContainer) LinearLayout port2Conteiner;
	@InjectView(R.id.portTextBaseLine) View port2Baseline;

	private Device device;
	private ComandoDataAssembler comando;
	private static SendComandoAsyncTask sendComandoTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comando);
		handleIntent();
	}

	private void handleIntent() {
		device = (Device) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_KEY);
		comando = new ComandoDataAssembler();
		comando.setDevice(device);
		setDeviceOnView();
		initializeSpiners();
	}



	private void setDeviceOnView() {
		if(device.getName() != null){
			nameText.setText(device.getName());
		}else{
			nameText.setText(getString(R.string.no_data));
		}

		if(device.getDescription() != null){
			decriptionText.setText(device.getDescription());
		}else{
			decriptionText.setText(getString(R.string.no_data));
		}

		if(device.getCode() != null){
			codeText.setText(device.getCode());
		}else{
			codeText.setText(getString(R.string.no_data));
		}

		if(device.getDeviceType() != null){
			trackerTypeText.setText(device.getDeviceType().name());
		}else{
			trackerTypeText.setText(getString(R.string.no_data));
		}

	}

	private void initializeSpiners() {
		initializeComandoSelector();
		intializeTimeZoneSpiner();

	}

	private void intializeTimeZoneSpiner() {
		String[] timeZoneStringArray = {TimeZone.GMT_12.getLabel(),TimeZone.GMT_11.getLabel(),
				TimeZone.GMT_10.getLabel(),TimeZone.GMT_9.getLabel(),TimeZone.GMT_8.getLabel(),
				TimeZone.GMT_7.getLabel(),TimeZone.GMT_6.getLabel(),TimeZone.GMT_5.getLabel(),
				TimeZone.GMT_4.getLabel(),TimeZone.GMT_3.getLabel(),TimeZone.GMT_2.getLabel(),
				TimeZone.GMT_1.getLabel(),TimeZone.GMT.getLabel(),TimeZone.GMT1.getLabel(),
				TimeZone.GMT2.getLabel(),TimeZone.GMT3.getLabel(),TimeZone.GMT4.getLabel(),
				TimeZone.GMT5.getLabel(),TimeZone.GMT6.getLabel(),TimeZone.GMT7.getLabel(),
				TimeZone.GMT8.getLabel(),TimeZone.GMT9.getLabel(),TimeZone.GMT10.getLabel()
				,TimeZone.GMT11.getLabel(),TimeZone.GMT12.getLabel(),TimeZone.GMT13.getLabel()};

		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				ComandoActivity.this, android.R.layout.simple_spinner_item,
				timeZoneStringArray);

		timeZoneSelector.setAdapter(adapter);
		timeZoneSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> adpterView,
					View view, int position, long l) {

				if (TimeZone.fromLabel(adapter.getItem(position)) != null){
					comando.setTimeZone(TimeZone.fromLabel(adapter.getItem(position)).getValue());
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				return;
			}
		});

	}

	private void initializeComandoSelector() {
		String[] comandoStringArray = {getString(R.string.REQUEST_POSITION),getString(R.string.DISABLE_PANIC),
				getString(R.string.ANTI_THEFT),getString(R.string.DISABABLE_ANTI_THEFT),
				getString(R.string.TIME_ZONE),getString(R.string.OUTPUT),getString(R.string.APN_SETTINGS),
				getString(R.string.SERVER),getString(R.string.RESET)};

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				ComandoActivity.this, android.R.layout.simple_spinner_item,
				comandoStringArray);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		comandoSelector.setAdapter(adapter);
		comandoSelector
		.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> adpterView,
					View view, int position, long l) {

				setComandoType(position);
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				return;
			}
		});
	}

	private void setComandoType(int position){
		CommandTypeEnum type = CommandTypeEnum.fromValue(position);
		comando.setCommandTypeEnum(type);
		visualizeComandoFields(type);
	}

	private void visualizeComandoFields(CommandTypeEnum type) {
		switch (type) {
		case REQUEST_POSITION:
			setAllViewsGone();
			break;

		case DISABLE_PANIC:
			setAllViewsGone();

			break;

		case ANTI_THEFT:
			setAllViewsGone();
			setAntiTheftViewsVisible();
			break;

		case DISABABLE_ANTI_THEFT:
			setAllViewsGone();

			break;

		case TIME_ZONE:
			setAllViewsGone();
			setTimeZoneViewsVisible();
			break;

		case OUTPUT:
			setAllViewsGone();
			setOutPutViewsVisible();

			break;
		case APN:
			setAllViewsGone();
			setApnViewsVisible();
			break;

		case SERVER:
			setAllViewsGone();
			setServerViewVisible();

			break;

		case RESET:
			setAllViewsGone();

			break;

		default:
			break;
		}

	}

	private void setAllViewsGone() {
		setTimeZoneViewsInvisible();
		setAntiTheftViewsInvisible();
		setOutPutViewsInvisible();
		setApnViewsInvisible();
		setServerViewInvisible();

	}

	private void setTimeZoneViewsInvisible() {
		timeZoneConteiner.setVisibility(View.GONE);
		timeZoneBaseLine.setVisibility(View.GONE);
	}

	private void setTimeZoneViewsVisible() {
		timeZoneConteiner.setVisibility(View.VISIBLE);
		timeZoneBaseLine.setVisibility(View.VISIBLE);
	}

	private void setAntiTheftViewsInvisible() {
		antiTheftConteiner.setVisibility(View.GONE);
		antiTheftBaseline.setVisibility(View.GONE);
	}

	private void setAntiTheftViewsVisible() {
		antiTheftConteiner.setVisibility(View.VISIBLE);
		antiTheftBaseline.setVisibility(View.VISIBLE);
	}

	private void setOutPutViewsInvisible() {
		saida1Conteiner.setVisibility(View.GONE);
		saida1Baseline.setVisibility(View.GONE);
		saida2Conteiner.setVisibility(View.GONE);
		saida2Baseline.setVisibility(View.GONE);
		saida3Conteiner.setVisibility(View.GONE);
		saida3Baseline.setVisibility(View.GONE);
	}

	private void setOutPutViewsVisible() {
		saida1Conteiner.setVisibility(View.VISIBLE);
		saida1Baseline.setVisibility(View.VISIBLE);
		saida2Conteiner.setVisibility(View.VISIBLE);
		saida2Baseline.setVisibility(View.VISIBLE);
		saida3Conteiner.setVisibility(View.VISIBLE);
		saida3Baseline.setVisibility(View.VISIBLE);
	}

	private void setApnViewsInvisible() {
		urlConteiner.setVisibility(View.GONE);
		urlBaseline.setVisibility(View.GONE);
		userConteiner.setVisibility(View.GONE);
		userBaseline.setVisibility(View.GONE);
		passConteiner.setVisibility(View.GONE);
		passBaseline.setVisibility(View.GONE);
	}

	private void setApnViewsVisible() {
		urlConteiner.setVisibility(View.VISIBLE);
		urlBaseline.setVisibility(View.VISIBLE);
		userConteiner.setVisibility(View.VISIBLE);
		userBaseline.setVisibility(View.VISIBLE);
		passConteiner.setVisibility(View.VISIBLE);
		passBaseline.setVisibility(View.VISIBLE);
	}

	private void setServerViewInvisible() {
		server1Conteiner.setVisibility(View.GONE);
		server1Baseline.setVisibility(View.GONE);
		portConteiner.setVisibility(View.GONE);
		portBaseline.setVisibility(View.GONE);
		server2Container.setVisibility(View.GONE);
		server2Baseline.setVisibility(View.GONE);
		port2Conteiner.setVisibility(View.GONE);
		port2Baseline.setVisibility(View.GONE);
	}

	private void setServerViewVisible() {
		server1Conteiner.setVisibility(View.VISIBLE);
		server1Baseline.setVisibility(View.VISIBLE);
		portConteiner.setVisibility(View.VISIBLE);
		portBaseline.setVisibility(View.VISIBLE);
		server2Container.setVisibility(View.VISIBLE);
		server2Baseline.setVisibility(View.VISIBLE);
		port2Conteiner.setVisibility(View.VISIBLE);
		port2Baseline.setVisibility(View.VISIBLE);
	}

	class SendComandoAsyncTask extends RoboAsyncTask<RestClient> {

		protected  ProgressDialog dialog;

		protected SendComandoAsyncTask(Context context) {
			super(context);
			dialog =ProgressDialog.show(context, "", getString(R.string.processing), true, false);
			dialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() throws Exception {
			super.onPreExecute();
			dialog.setMessage(context.getString(R.string.processing));
			dialog.show();
		}

		public RestClient call() throws Exception {
			ComandoCloud cloud = new ComandoCloud();
			cloud.sendCommandToServer(context, comando);
			return cloud;
		}

		@Override
		protected void onSuccess(RestClient t) throws Exception {
			if (dialog.isShowing()) {
				dialog.cancel();
				dialog = null;
			}
			sendComandoTask = null;
			if(t.getResponseCode()== HttpStatus.SC_OK){
				finish();
			}else{
				AppHelper.getInstance().presentError(context, getString(R.string.Erro), getString(R.string.error_comand));
			}
		}

	}

	public void onClickSendComand(View view){
		setContentsFromViewToComando();
	}

	private void setContentsFromViewToComando() {
		switch (comando.getCommandTypeEnum()) {
		case REQUEST_POSITION:
			startComandoAsyncTask();
			break;

		case DISABLE_PANIC:
			startComandoAsyncTask();
			break;

		case ANTI_THEFT:
			comando.setActivate(antiTheftBox.isChecked());
			startComandoAsyncTask();
			break;

		case DISABABLE_ANTI_THEFT:
			startComandoAsyncTask();
			break;

		case TIME_ZONE:
			startComandoAsyncTask();
			break;

		case OUTPUT:
			Outputs exits = new Outputs();
			exits.setExit1(saida1Box.isChecked()? 0:1);
			exits.setExit2(saida2Box.isChecked()? 0:1);
			exits.setExit3(saida3Box.isChecked()? 0:1);
			comando.setExits(exits);
			startComandoAsyncTask();

			break;
		case APN:
			if(checkApnFields()){
				ApnSettings apn = new ApnSettings();
				apn.setPassword(passText.getText().toString());
				apn.setUrl(urlText.getText().toString());
				apn.setUsername(userText.getText().toString());
				comando.setApnSettings(apn);
				startComandoAsyncTask();
			}else{
				AppHelper.getInstance().presentError(this, getString(R.string.error_missing_info), passText.getHint()+" , "+urlText.getHint() +" , "+userText.getHint() );
			}
			break;

		case SERVER:
			if(checkServerFields()){
				ServerSettings server = new ServerSettings();
				server.setPrimaryIp(server1Text.getText().toString());
				server.setPrimaryPort(portText.getText().toString());
				server.setSeconderyIp(server2Text.getText().toString());
				server.setSeconderyPort(port2Text.getText().toString());
				comando.setServerSettings(server);
				startComandoAsyncTask();
			}else{
				AppHelper.getInstance().presentError(this, getString(R.string.error_missing_info),  server1Text.getHint()+" , "+portText.getHint() +" , "+server2Text.getHint()+" , "+port2Text.getHint());
			}
			break;

		case RESET:
			startComandoAsyncTask();
			break;

		default:
			break;
		}


	}

	private void startComandoAsyncTask() {
		if(sendComandoTask == null){
			sendComandoTask = new SendComandoAsyncTask(this);
		}

		sendComandoTask.execute();
	}

	private boolean checkServerFields() {
		if(server1Text.length() == 0||portText.length() == 0||server2Text.length() == 0||port2Text.length() == 0)
			return false;

		return true;
	}

	private boolean checkApnFields() {
		if(urlText.length() == 0||userText.length() == 0||passText.length() == 0)
			return false;

		return true;
	}


}
