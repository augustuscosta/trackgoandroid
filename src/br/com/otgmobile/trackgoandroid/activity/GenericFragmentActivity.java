package br.com.otgmobile.trackgoandroid.activity;

import java.sql.SQLException;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.model.Event;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;
import br.com.otgmobile.trackgoandroid.util.BroadCastDictionary;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;
import br.com.otgmobile.trackgoandroid.util.LogUtil;
import br.com.otgmobile.trackgoandroid.util.NotificationUtil;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;

public class GenericFragmentActivity<H extends OrmLiteSqliteOpenHelper> extends FragmentActivity{

	private volatile H helper;
	private volatile boolean created = false;
	private volatile boolean destroyed = false;


	public H getHelper() {
		if (helper == null) {
			if (!created) {
				throw new IllegalStateException("A call has not been made to onCreate() yet so the helper is null");
			} else if (destroyed) {
				throw new IllegalStateException(
						"A call to onDestroy has already been made and the helper cannot be used after that point");
			} else {
				throw new IllegalStateException("Helper is null for some unknown reason");
			}
		} else {
			return helper;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (helper == null) {
			helper = getHelperInternal(this);
			created = true;
		}
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		registerReceiver(listener, new IntentFilter(ConstUtil.FINISH_ACTIVITY));
		registerReceiver(eventListener, new IntentFilter(BroadCastDictionary.NEW_EVENT.getValue()));
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		unregisterReceiver(listener);
		unregisterReceiver(eventListener);
		super.onPause();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if ( ConstUtil.INVALID_TOKEN == resultCode) {
			setResult(resultCode, data);
			finish();
		}
	}
	
	@Override
    protected void onDestroy() {
            super.onDestroy();
            releaseHelper(helper);
            destroyed = true;
    }

	protected H getHelperInternal(Context context) {
		@SuppressWarnings({ "unchecked", "deprecation" })
		H newHelper = (H) OpenHelperManager.getHelper(context);
		return newHelper;
	}
	
	protected void releaseHelper(H helper) {
        OpenHelperManager.releaseHelper();
        helper = null;
}
	
	final private BroadcastReceiver listener = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if ( ConstUtil.FINISH_ACTIVITY.equals(intent.getAction()) ) {
				final int resultCode = intent.getIntExtra(ConstUtil.RESULT_CODE, -1);
				if ( resultCode == -1 ) {
					return;
				}

				GenericFragmentActivity.this.setResult(resultCode, intent);
				GenericFragmentActivity.this.finish();
			}
		}
	};
	
	final private BroadcastReceiver eventListener = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			EventHistoryDetail eventHistoryDetail;
			if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
				eventHistoryDetail = (EventHistoryDetail) intent.getSerializableExtra(ConstUtil.SERIALIZABLE_KEY);
				persitEventAndShowDialog(eventHistoryDetail);
			}
		}
	};


	protected void persitEventAndShowDialog(EventHistoryDetail eventHistoryDetail) {
		try {
		if(eventHistoryDetail.getEvent()!= null){
				getHelper().getDao(Event.class).createIfNotExists(eventHistoryDetail.getEvent());
		}
		getHelper().getDao(EventHistoryDetail.class).createIfNotExists(eventHistoryDetail);
		showNewOccurenceDialog(eventHistoryDetail);
		} catch (SQLException e) {
			LogUtil.e("erro ao persistir evento", e);
		}
		
	}
	
	protected void showNewOccurenceDialog(final EventHistoryDetail eventHistoryDetail) {

		if (eventHistoryDetail == null)
			return;

		final Dialog dialog = new Dialog(this);

		OnClickListener confirmButtonListener = new View.OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
				NotificationUtil.removeNotification(GenericFragmentActivity.this);
				gotoMapLocation(eventHistoryDetail);
			}

		};
		
		OnClickListener desmissButtonListener = new View.OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
				NotificationUtil.removeNotification(GenericFragmentActivity.this);
			}
			
		};

		dialog.setContentView(R.layout.event_alert);
		final Button ok = (Button) dialog.findViewById(R.id.bt_ok);
		final Button cancel = (Button) dialog.findViewById(R.id.bt_cancel);
		final TextView name = (TextView) dialog.findViewById(R.id.eventName);
		final TextView description = (TextView) dialog
				.findViewById(R.id.eventDescription);

		name.setText(eventHistoryDetail.getEvent().getName());
		description.setText(eventHistoryDetail.getEvent().getDescription());
		ok.setOnClickListener(confirmButtonListener);
		cancel.setOnClickListener(desmissButtonListener);
		dialog.show();
		NotificationUtil.createNewOcurenceNotification(eventHistoryDetail, this);
	}

	protected void gotoMapLocation(EventHistoryDetail eventHistoryDetail) {
		Intent intent = new Intent(this, EventLocaterMapActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, eventHistoryDetail);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.EVENT_MAP);
	}

}
