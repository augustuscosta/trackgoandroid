package br.com.otgmobile.trackgoandroid.activity;

import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.service.SocketService;


public class MainTabActivity extends GenericTabActivity {
	
	@InjectView(android.R.id.tabhost) TabHost tabHost;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_tab);
		startServices();
		createTabs();
	}


	private void createTabs() {
		
		tabHost = getTabHost();
		Intent intent = new Intent(MainTabActivity.this,VehicleMapActivity.class);
		addTab(R.string.vehicle_map, R.drawable.veiculos, intent, "mapTab");
		Intent it = new Intent(MainTabActivity.this,EventListActivity.class);
		addTab(R.string.events, R.drawable.eventos, it, "eventTab");
		
		
	}
	
	private void addTab(int labelId, int drawableId, Intent intent, String tabName){
		TabSpec spec = tabHost.newTabSpec(tabName);		
		
		View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
		
		TextView title = (TextView) tabIndicator.findViewById(R.id.title);
		title.setText(labelId);
		ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
		icon.setImageResource(drawableId);
		
		spec.setIndicator(tabIndicator);
		spec.setContent(intent);
		tabHost.addTab(spec);
	}
	
	private void startServices() {
		SocketService.startService(this);
	}
	
	@Override
	protected void onDestroy() {
		SocketService.stopService(this);
		super.onDestroy();
	}

}
