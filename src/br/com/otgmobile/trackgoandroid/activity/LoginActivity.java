package br.com.otgmobile.trackgoandroid.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.cloud.RestClient;
import br.com.otgmobile.trackgoandroid.cloud.UserCloud;
import br.com.otgmobile.trackgoandroid.model.User;
import br.com.otgmobile.trackgoandroid.util.AppHelper;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;
import br.com.otgmobile.trackgoandroid.util.LogUtil;
import br.com.otgmobile.trackgoandroid.util.Session;

public class LoginActivity extends RoboActivity{

	@InjectView(R.id.userName) EditText userName;
	@InjectView(R.id.userPass) EditText userPassword;
	@InjectView(R.id.login) Button loginButton;
	private static LoginTask loginTask;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		userPassword.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					processLogin();
					return true;
				}
				
				return true;
			}
		});
		LogUtil.initLog(this, null);
		checkToken();
	}


	public void onClickLogin(View view){
		processLogin();

	}


	private void processLogin() {
		if(userName.length() == 0 || userPassword.length() == 0){
			AppHelper.getInstance().presentError(this,getString(R.string.app_name),getString(R.string.error_missing_info_login));
		}else{
			executeLoginTask();
		}
	}

	private void executeLoginTask() {
		stopLoginTask();
		loginTask = new LoginTask(this);			
		loginTask.execute();
	}

	private void stopLoginTask() {
		if(loginTask != null){
			loginTask.cancel(true);
			loginTask = null;
		}
		
	}

	private class LoginTask extends AsyncTask<String, Void, RestClient> {
		ProgressDialog mDialog;
		Context context;
		String erroMessage = null;

		UserCloud cloud;
		

		LoginTask(Context context) {
			loginButton.setEnabled(false);
			this.context = context;
			mDialog = ProgressDialog.show(context, "", getString(R.string.processing), false, true);
			mDialog.setCancelable(true);
			mDialog.setOnCancelListener(listener);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		@Override
		public RestClient doInBackground(String... params){
			return login();
		}

		RestClient login(){
			cloud = new UserCloud();
			try {
				User user = new User();
				user.setNome(userName.getText().toString());
				user.setSenha(userPassword.getText().toString());
				cloud.login(context,user);
			} catch (Throwable e) {
				erroMessage = e.getLocalizedMessage();
				return cloud;
			}
			if(cloud.getResponseCode() != 200){
				erroMessage = cloud.getErrorMessage();
				return cloud;
			}
			return cloud;
		}

		@Override
		public void onPostExecute(RestClient cloud) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			loginButton.setEnabled(true);
			loginTask = null;
			mDialog.dismiss();
			if (erroMessage != null) {
				if(cloud.getResponseCode() == ConstUtil.INVALID_TOKEN){
					AppHelper.getInstance().presentError(context,getResources().getString(R.string.error_conection),getResources().getString(R.string.wrong_user_or_password));
				}else{
					AppHelper.getInstance().presentError(context,getResources().getString(R.string.error_conection),erroMessage);
				}
			} else {
				checkToken();
			}
		}


	}

	private void checkToken() {
		if(Session.getToken(this) != null && Session.getToken(this).length() > 0){
					Intent intent = new Intent(LoginActivity.this, MainTabActivity.class);
					startActivity(intent);
		}else{
			Intent intent = new Intent(LoginActivity.this, SplashScreen.class);
			startActivity(intent);
			
		}
	}
	
	private OnCancelListener listener = new OnCancelListener() {
		
		public void onCancel(DialogInterface arg0) {
			if(loginTask != null){
				loginTask.cancel(true);
 			}
		}
	};


}
