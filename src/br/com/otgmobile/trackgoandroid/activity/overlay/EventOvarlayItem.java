package br.com.otgmobile.trackgoandroid.activity.overlay;

import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class EventOvarlayItem extends OverlayItem{
	
	private EventHistoryDetail event;

	public EventOvarlayItem(GeoPoint point, String title, String snippet, EventHistoryDetail event) {
		super(point, title, snippet);
		this.event = event;
	}

	public EventHistoryDetail getEvent() {
		return event;
	}

	
}
