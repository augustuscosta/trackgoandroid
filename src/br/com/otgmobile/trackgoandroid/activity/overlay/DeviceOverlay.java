package br.com.otgmobile.trackgoandroid.activity.overlay;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import br.com.otgmobile.trackgoandroid.model.Device;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;

public class DeviceOverlay extends ItemizedOverlay<DeviceOvarlayItem>{
	private ArrayList<DeviceOvarlayItem> mOverlays = new ArrayList<DeviceOvarlayItem>();
	private DeviceOverLayCallback callBack;
	private static final int FONT_SIZE = 24;
    private static final int TITLE_MARGIN = 6;
    private int markerHeight;

	public DeviceOverlay(Drawable defaultMarker,DeviceOverLayCallback callBack) {
		super(boundCenterBottom(defaultMarker));
		this.callBack = callBack;
		  markerHeight = ((BitmapDrawable) defaultMarker).getBitmap().getHeight();
	}

	public DeviceOverlay(DeviceOverLayCallback callBack) {
		super(null);
		this.callBack = callBack;
	}

	public void addOverlay(DeviceOvarlayItem item){
		mOverlays.add(item);
		setLastFocusedIndex(-1);
		populate();
	}

	@Override
	protected DeviceOvarlayItem createItem(int i) {
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

	@Override
	protected boolean onTap(int index) {
		Device device = mOverlays.get(index).getDevice();
		callBack.deviceSelected(device);			
		return super.onTap(index);
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		super.draw(canvas, mapView, shadow);
		
		for(DeviceOvarlayItem item :mOverlays){
			GeoPoint point = item.getPoint();
            Point markerBottomCenterCoords = new Point();
            mapView.getProjection().toPixels(point, markerBottomCenterCoords);
            
            TextPaint paintText = new TextPaint();
            Paint paintRect = new Paint();
            Rect rect = new Rect();
            paintText.setTextSize(FONT_SIZE);
            
            rect.inset(-TITLE_MARGIN, -TITLE_MARGIN);
            rect.offsetTo(markerBottomCenterCoords.x - rect.width()/2, markerBottomCenterCoords.y - markerHeight - rect.height());
            
            paintText.setTextAlign(Paint.Align.CENTER);
            paintText.setTextSize(FONT_SIZE);
            paintText.setTypeface(Typeface.DEFAULT_BOLD);
            paintText.setARGB(100, 0, 0, 0);
            paintRect.setARGB(130, 0, 0, 0);
//            canvas.drawText(item.getTitle(), rect.left + rect.width() / 2,
//                    rect.bottom - TITLE_MARGIN, paintText);
		}
	}

}
