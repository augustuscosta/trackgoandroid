package br.com.otgmobile.trackgoandroid.activity.overlay;

import br.com.otgmobile.trackgoandroid.model.Device;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class DeviceOvarlayItem extends OverlayItem{
	
	private Device device;

	public DeviceOvarlayItem(GeoPoint point, String title, String snippet, Device device) {
		super(point, title, snippet);
		this.device = device;
	}

	public Device getDevice() {
		return device;
	}

	
}
