package br.com.otgmobile.trackgoandroid.activity.overlay;

import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;

public interface EventOverLayCallback {
	void eventSelected(EventHistoryDetail event);
}
