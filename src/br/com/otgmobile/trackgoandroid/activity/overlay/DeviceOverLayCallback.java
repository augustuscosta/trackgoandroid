package br.com.otgmobile.trackgoandroid.activity.overlay;

import br.com.otgmobile.trackgoandroid.model.Device;

public interface DeviceOverLayCallback {
	void deviceSelected(Device device);
}
