package br.com.otgmobile.trackgoandroid.activity;

import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackgoandroid.R;
import br.com.otgmobile.trackgoandroid.database.DatabaseHelper;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.TrackerData;
import br.com.otgmobile.trackgoandroid.util.ConstUtil;

public class DeviceDetailActivity extends GenericActivity<DatabaseHelper> {

	private Device device;

	@InjectView(R.id.device_detail_container) LinearLayout deviceLinearLayout;
	@InjectView(R.id.name_text) TextView nameText;
	@InjectView(R.id.description_text) TextView descriptionText;
	@InjectView(R.id.code_text) TextView codeText;
	@InjectView(R.id.tracker_type_text) TextView trackerTextView;
	@InjectView(R.id.latitude_text) TextView latitudeText;
	@InjectView(R.id.longitude_text) TextView longitudeText;
	@InjectView(R.id.speed_text) TextView speedText;
	@InjectView(R.id.rpm_text) TextView rpmText;
	@InjectView(R.id.panic_text) TextView panicText;
	@InjectView(R.id.date_text) TextView dateText;
	@InjectView(R.id.altitude_text) TextView altitudeText;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_detail);
		handleIntent();
	}

	private void handleIntent() {
		device = (Device) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_KEY);
		setDeviceOnView();
	}

	private void setDeviceOnView() {
		if(device == null){
			return;
		}
		setDeviceRawData();
		setTrackerDataOnView(device.getTrackeData());
	}


	private void setDeviceRawData() {
		if(device.getName() != null){
			nameText.setText(device.getName());
		}else{
			nameText.setText(getString(R.string.no_data));
		}

		if(device.getDescription() != null){
			descriptionText.setText(device.getDescription());
		}else{
			descriptionText.setText(getString(R.string.no_data));
		}

		if(device.getCode() != null){
			codeText.setText(device.getCode());
		}else{
			codeText.setText(getString(R.string.no_data));
		}

		if(device.getDeviceType() != null){
			trackerTextView.setText(device.getDeviceType().name());
		}else{
			trackerTextView.setText(getString(R.string.no_data));
		}
	}
	
	private void setTrackerDataOnView(TrackerData trackerData) {
		if(trackerData == null){
			setNoDataForAllTrackingFields();
		}else{
			setLatitudeAndLongitudeOnView(trackerData);
			setSpeedOnView(trackerData);
			setIgintionText(trackerData);
			setPanicText(trackerData);
			setDateOnView(trackerData);
			setAltitudeOnView(trackerData);
		}
	}

	private void setNoDataForAllTrackingFields() {
		latitudeText.setText(getString(R.string.no_data));;
		longitudeText.setText(getString(R.string.no_data));;
		speedText.setText(getString(R.string.no_data));;
		rpmText.setText(getString(R.string.no_data));;
		panicText.setText(getString(R.string.no_data));;
		dateText.setText(getString(R.string.no_data));;
	}

	private void setLatitudeAndLongitudeOnView(TrackerData trackerData) {
		if(trackerData.getLatitude()!= null){
			latitudeText.setText(trackerData.getLatitude().toString());
		}else{
			latitudeText.setText(getString(R.string.no_data));;				
		}

		if(trackerData.getLongitude()!= null){
			longitudeText.setText(trackerData.getLongitude().toString());
		}else{
			longitudeText.setText(getString(R.string.no_data));;				
		}
	}

	private void setSpeedOnView(TrackerData trackerData) {
		if(trackerData.getSpeed()!= null){
			speedText.setText(trackerData.getSpeed().toString());
		}else{
			speedText.setText(getString(R.string.no_data));;				
		}
	}

	private void setIgintionText(TrackerData trackerData) {
		if(trackerData.getIgnition()!= null){
			if(trackerData.getIgnition()){
				rpmText.setText(getString(R.string.on_f));;									
			}else{
				rpmText.setText(getString(R.string.off_f));;									
			}
		}else{
			rpmText.setText(getString(R.string.no_data));;				
		}
	}

	private void setPanicText(TrackerData trackerData) {
		if(trackerData.getPanic()!= null){
			if(trackerData.getPanic()){
				panicText.setText(getString(R.string.on));;									
			}else{
				panicText.setText(getString(R.string.off));;									
			}
		}else{
			panicText.setText(getString(R.string.no_data));;				
		}
	}

	private void setDateOnView(TrackerData trackerData) {
		if(trackerData.getDate() != null){
			dateText.setText(trackerData.getDate().toLocaleString());
		}else{
			dateText.setText(getString(R.string.no_data));;				
		}
	}

	private void setAltitudeOnView(TrackerData trackerData) {
		if(trackerData.getAltitude() != null){
			altitudeText.setText(trackerData.getAltitude().toString());
		}else{
			altitudeText.setText(getString(R.string.no_data));;				
		}
	}



	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public void onClickSendComand(View view){
		Intent intent = new Intent(DeviceDetailActivity.this,ComandoActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, device);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.COMANDO);
	}

}
