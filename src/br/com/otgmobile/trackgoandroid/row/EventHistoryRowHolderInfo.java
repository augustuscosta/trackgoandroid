package br.com.otgmobile.trackgoandroid.row;

import android.widget.TextView;
import br.com.otgmobile.trackgoandroid.model.Device;
import br.com.otgmobile.trackgoandroid.model.Event;
import br.com.otgmobile.trackgoandroid.model.EventHistoryDetail;

public class EventHistoryRowHolderInfo {
	
	public TextView deviceTextView;
	public TextView dateTextView;
	public TextView eventDescriptionTextView;
	
	public void drawRow(EventHistoryDetail obj){
		if(obj == null) return;
		Device device = obj.getDevice();
		Event event = obj.getEvent();
		
		if(device != null && device.getCode() != null){
			deviceTextView.setText(device.getName());
	
		}else{
			deviceTextView.setText("");
		}
		
		if(event!= null && event.getName()!= null){
			eventDescriptionTextView.setText(event.getName());
		
		}else{
			eventDescriptionTextView.setText("");
		}
		
		if(obj.getMetricDate()!= null){
			dateTextView.setText(obj.getMetricDate().toLocaleString());
		
		}else{
			dateTextView.setText("");
		}
		
	}

}
